const express = require('express'),
      backend = express(),
      bodyParser = require('body-parser'),
      mongoose = require('mongoose');

const DB ="mongodb://192.168.75.128:27017/music_app"; 

mongoose.connect(DB);

backend.use(express.json());

// MONGOOSE SET UP
const musicSchema = new mongoose.Schema({
    album: {type: String, required: false},
    artist: {type: String, required: false},
    year: {type: Number, required: false},
    artwork: {type: String, required: false}
});

const Album = mongoose.model("Album", musicSchema);


// ROUTES
// INDEX ROUTE
backend.get('/', function(req, res){
    res.redirect("/music");
})

// SHOW ROUTES
// GET ALL
backend.get('/music', function(req, res){
    Album.find({}, function(err, albums){
        if(err){
            return res
                .status(400)
                .json({success: false, err: err});
        }
        if(!albums.length){
            return res
                .status(404)
                .json({success: false, error: 'No albums found'});
        }
        return res
            .status(200)
            .json({success: true, data: albums})
    })
})

// GET ONE
backend.get('/music/:id', function(req,res){
    Album.findById(req.params.id, function(err, album){
        if(err){
            return res
                .status(400)
                .json({success: false, error:err});
        }
        if(!album){
            return res
                .status(404)
                .json({success: false, error: "Album not found"});
        }
        return res
            .status(200)
            .json({success: true, data: album});
    })
})

// CREATE ROUTE
backend.post('/music', function(req,res){

    const body = req.body;
    if(!body){
        return res
            .status(400)
            .json({success: false, error: "You must specify album information"});
    }

    const album = new Album(body);

    if(!album){
        return res
            .status(400)
            .json({success: false, error:"Album creation failed"});
    }

    album
        .save()
        .then(()=>{
            return res
                .status(201)
                .json({
                    success: true,
                    id: album._id,
                    message: "Album created."
                })
        })
        .catch(error =>{
            return res
                .status(400)
                .json({error, message: "Album not created"});
        })


});
// UPDATE ROUTE
backend.put('/music/:id', function(req, res){

    const body = req.body;
    if(!body){
        return res
            .status(400)
            .json({success:false, error: "You must provide some data to update."})
    }

    // find the document to be updated in the database
    Album.findOne({_id: req.params.id}, (err, album) =>{

        if(err){
            return res
                .status(400)
                .json({success: false, error: err});
        }
        if(!album){
            return res
                .status(404)
                .json({success: false, error:"Album not found"});
        }

        // update all the info in the browser album from the body album
        album.album = body.album;
        album.artist = body.artist;
        album.year = body.year;
        album.artwork = body.artwork;

        album
            .save()
            .then(() =>{
                return res
                    .status(200)
                    .json({
                        success: true,
                        id: album._id,
                        message: "Album updated."
                    })
            })
            .catch(error => {
                return res
                    .status(404)
                    .json({error, message: "Album not updated."})
            });
    });
});
// DELETE ROUTE
backend.delete('/music/:id', function(req, res){

    Album.findOneAndDelete({_id: req.params.id}, function(err, album){
        if(err){
            return res
                .status(400)
                .json({success: false, error: err});
        }
        if(!album){
            return res
                .status(404)
                .json({success: false, error: "Album not found"});
        }
        return res
            .status(200)
            .json({success: true, data: album});
    });
});

backend.listen(3001, function(){
    console.log("Server started successfully")
})

